import ldap
import auth
LDAP_SERVER = 'ldap://www.zflexldap.com'
LDAP_USERNAME = 'uid=guest1,ou=users,ou=guests,dc=zflexsoftware,dc=com'
LDAP_PASSWORD = 'guest1password'
ldap_client = ldap.initialize(LDAP_SERVER)
# perform a synchronous bind
ldap_client.set_option(ldap.OPT_REFERRALS,0)
session = auth.check_credentials(LDAP_USERNAME, LDAP_PASSWORD)
print(session)
#ldap_client.simple_bind_s(LDAP_USERNAME, LDAP_PASSWORD)
