from flask import Flask, request, jsonify
app = Flask(__name__)
import ldap
from auth import test_check_credentials

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/testlogin', methods=['POST'])
def login():
    error = None
    status = None
    username = None
    if request.method == 'POST':
      content = request.json
      session  = test_check_credentials(content["username"], content["password"])
      return jsonify(session)

@app.route('/login', methods=['POST'])
def login():
    error = None
    status = None
    username = None
    if request.method == 'POST':
      content = request.json
      session  = check_credentials(content["username"], content["password"])
      return jsonify(session)